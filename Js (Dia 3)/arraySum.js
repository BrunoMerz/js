//Recebe n arrays e soma todos os seu elementos
//Input: [1,2,3] [1,2,2] [1,1]
//Output: 13
function sum(...n){
    var sum = 0
    for(y of n){
        for(x of y){
            sum += x
        }
    }
    return sum
}

console.log(sum([1,2,3],[1,2,2],[1,1]))