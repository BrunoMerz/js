//[1, 2, 3] [3, 3, 7] [9, 111, 3] => [3]

//b.includes(a) vvvvv
/*
function ifExistsIn(a,b){
    for(i in b){
        if(a == b[i]){
            return true
        }
    }
    return false
}
*/
//Remove Duplicatas
function removeDuplicate(Lista){
    var newLista = []
    for(x in Lista){
        if(newLista.includes(Lista[x]) == false){
            newLista.push(Lista[x])
        }
    }
    return newLista
}
// se X existe em todas y1[],...,yn[]
function existsInAll(x,n){
    for(i = 1;i < n.length; i++){
        if(n[i].includes(x) == false){
            return false
        }
    }
    return true
}

//Input são varias listas[];
//Output é uma lista[] com os elementos que existem em -todas- as listas input[]
//Esse método que eu fiz usa os elementos da primeira lista como base para checar todas as outras para salvar tempo (Já que se não existe na primeira lista, não será output[]).
//Para ser mais eficiente ainda em casos de um sample grande, escolher a lista de menor .lenght para se a base.(não feito na função abaixo)
function arrayExclusive(...n){
    finalarray = []
    template = removeDuplicate(n[0])
    for(x of template){
        if(existsInAll(x,n) == true){
        finalarray.push(x)
        }
    }
    return finalarray
}

console.log(arrayExclusive([1, 2, 3, 9],[3, 2, 7,9],[9, 11, 3, 2],[2,1,9])) //output: [2,9]