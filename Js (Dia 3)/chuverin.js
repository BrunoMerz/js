//(x,[a,b]) = [xa,xb]
function distributiva(x,n){
    let res = n.map(e => e * x)
    return res
}

console.log(distributiva(2, [1,3,6,10]))