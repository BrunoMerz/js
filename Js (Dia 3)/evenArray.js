//input é n[] de x elementos, são todos somados e retorna True se for Par
function arraySumEven(n){
    sum = 0
    for(x of n){
        sum += x
    }
    if(sum % 2 == 0){
        return true
    }else{
        return false
    }
}

//Itera sob as listas passadas na função e retorna aquelas que são par
function evenArray(...n){
    output = []
    for(valor of n){
        if(arraySumEven(valor) == true){
            output.push(valor)
        }
    }
    return output
}

//console.log(arraySumEven([1,1,2,4]))
console.log(evenArray([1, 1, 3],[1, 2, 2, 2, 3],[2]))