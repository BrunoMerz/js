//Input
//Lista de alunos com N turmas e N alunos no formato abaixo
//Resultado esperado:
//Paulo, Jonathan, Fred e Marcos, das turmas A, B, C e F
var alunos =
[
    {
        "nome": "Paulo",
        "turma": "A",
        "nota1": 9,
        "nota2": 9
    },
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "B",
        "nota1": 9,
        "nota2": 9
    },
    {
        "nome": "Jonh",
        "turma": "C",
        "nota1": 6,
        "nota2": 9
    },
    {
        "nome": "Fred",
        "turma": "C",
        "nota1": 7,
        "nota2": 9
    },
    {
        "nome": "Marcos",
        "turma": "F",
        "nota1": 2,
        "nota2": 5
    },
    {
        "nome": "Marcelo",
        "turma": "F",
        "nota1": 2,
        "nota2": 3
    }
]

//Se o elemento 'a' existe dentro da lista b[], return true, else, false.
function ifclassexist(a,b){
    for(i in b){
        if(a == b[i]){
            return true
        }
    }
    return false
}

//Input é um array no formato dos exmplos nas primeiras linhas, onde os alunos com maior média são guardados numa lista e retornado.
function maiormedia(lista){
    var melhores = [] //lista dos melhores alunos de cada turma
    var classes = [] //Lista das turmas contidas no Input (Supporta infinitas turmas e infinitos alunos)
    for(x in lista){
        //Se não há registro de um aluno da turma x em classes[]...
        if(ifclassexist(lista[x].turma,classes) == false){
            classes.push(lista[x].turma) //...adiciona a turma em classes[]...
            melhores.push(lista[x])//... e o aluno na melhores[]

        //caso haja, itera sob a melhores[] e compara um á um.
        }else{
            for(i in melhores){               //Quando acha um de turma igual, compara as médias -
                if(lista[x].turma == melhores[i].turma && (lista[x].nota1 + lista[x].nota2) > (melhores[i].nota1 + melhores[i].nota2)){
                    melhores.splice(i,1)      //- e subistitui caso média seja maior
                    melhores.push(lista[x])
                }
            }
        }
    }
    return(melhores)
}
//Dá print dos melhores alunos
function printmelhores(mAlunos){
    for(x in mAlunos){
        var media = (mAlunos[x].nota1 + mAlunos[x].nota2)
        media = media / 2
        console.log("O aluno "+ mAlunos[x].nome + " teve a maior média da turma " + mAlunos[x].turma + ", com "+ (((mAlunos[x].nota1 + mAlunos[x].nota2)/2)))
    }
}

printmelhores(maiormedia(alunos))