function ifexists(e,l){
    for(x in l){
        if(e == l[x]){
            return(x)
        }
    }
    return false
}

function ifsimilar(l1,l2){
    var l2copy = [2,3,1,1]
    for(x in l1){
        var resultOfComp = ifexists(l1[x],l2copy)
        if(resultOfComp !== false){
            l2copy.splice(resultOfComp,1)
        }
        else{
            return false
        }
    }
    if(l2copy.length == 0){
        return true
    }
    else{
        return false
    }
}

var a = [2,1,3,1]
var b = [2,3,1,1]
console.log(ifsimilar(a,b))