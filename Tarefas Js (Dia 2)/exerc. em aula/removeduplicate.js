// [1, 2, 2, 3, 7, 4, 3, 2, 1] => [1, 2, 3, 4, 7]
var num = [1, 2, 2, 3, 7, 4, 3, 2, 1]

//Se A existem em B[], Return true, else, False
//.includes() faz esse mesmo trabalho
/*
function ifExistsIn(a,b){
    for(i in b){
        if(a == b[i]){
            return true
        }
    }
    return false
}
*/
function removeDuplicate(Lista){
    var newLista = []
    for(x of Lista){
        if(newLista.includes(x) == false){
            newLista.push(x)
        }
    }
    return newLista
}

console.log(removeDuplicate(num))