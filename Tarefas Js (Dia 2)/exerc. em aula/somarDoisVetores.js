//input = [[x1,y1],...,[xn,yn]]
//output = [x1+...+xn, y1+...+yn]

//samples
input = [[2,3],[4,5],[3,6]]
input2 = [[-2,3],[5,7],[10,20],[28,49]]

function sum2d(lista){
    sum = [0,0]
    for(n in lista){
        sum[0] += lista[n][0]
        sum[1] += lista[n][1]
    }
    return sum
}

console.log(sum2d(input))